# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marek Laane <qiilaq69@gmail.com>, 2016, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-06 00:38+0000\n"
"PO-Revision-Date: 2020-04-24 13:19+0300\n"
"Last-Translator: Marek Laane <qiilaq69@gmail.com>\n"
"Language-Team: Estonian <kde-et@lists.linux.ee>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.12.3\n"

#: package/contents/ui/DeviceItem.qml:37
#, kde-format
msgid "Disconnect"
msgstr "Katkesta ühendus"

#: package/contents/ui/DeviceItem.qml:37
#, kde-format
msgid "Connect"
msgstr "Ühenda"

#: package/contents/ui/DeviceItem.qml:46
#, kde-format
msgid "Browse Files"
msgstr "Sirvi faile"

#: package/contents/ui/DeviceItem.qml:57
#, kde-format
msgid "Send File"
msgstr "Saada fail"

#: package/contents/ui/DeviceItem.qml:113
#, kde-format
msgid "Copy"
msgstr "Kopeeri"

#: package/contents/ui/DeviceItem.qml:209
#, kde-format
msgid "Yes"
msgstr "Jah"

#: package/contents/ui/DeviceItem.qml:209
#, kde-format
msgid "No"
msgstr "Ei"

#: package/contents/ui/DeviceItem.qml:215
#, kde-format
msgctxt "@label %1 is human-readable adapter name, %2 is HCI"
msgid "%1 (%2)"
msgstr ""

#: package/contents/ui/DeviceItem.qml:223
#, kde-format
msgid "Remote Name"
msgstr "Võrgunimi"

#: package/contents/ui/DeviceItem.qml:227
#, kde-format
msgid "Address"
msgstr "Aadress"

#: package/contents/ui/DeviceItem.qml:230
#, kde-format
msgid "Paired"
msgstr "Paardunud"

#: package/contents/ui/DeviceItem.qml:233
#, kde-format
msgid "Trusted"
msgstr "Usaldatav"

#: package/contents/ui/DeviceItem.qml:236
#, kde-format
msgid "Adapter"
msgstr "Adapter"

#: package/contents/ui/DeviceItem.qml:244
#, kde-format
msgid "Disconnecting"
msgstr "Ühenduse katkestamine"

#: package/contents/ui/DeviceItem.qml:244
#, kde-format
msgid "Connecting"
msgstr "Ühendumine"

#: package/contents/ui/DeviceItem.qml:250
#, fuzzy, kde-format
#| msgid "Connect"
msgid "Connected"
msgstr "Ühenda"

#: package/contents/ui/DeviceItem.qml:252
#, fuzzy, kde-format
#| msgctxt "Notification when the connection failed due to Failed"
#| msgid "Connection to the device failed"
msgid "Connection failed"
msgstr "Ühendumine seadmega nurjus"

#: package/contents/ui/DeviceItem.qml:259
#, kde-format
msgid "Audio device"
msgstr "Heliseade"

#: package/contents/ui/DeviceItem.qml:266
#, kde-format
msgid "Input device"
msgstr "Sisendseade"

#: package/contents/ui/DeviceItem.qml:270
#, kde-format
msgid "Phone"
msgstr ""

#: package/contents/ui/DeviceItem.qml:277
#, kde-format
msgid "File transfer"
msgstr "Failiedastus"

#: package/contents/ui/DeviceItem.qml:280
#, kde-format
msgid "Send file"
msgstr "Faili saatmine"

#: package/contents/ui/DeviceItem.qml:283
#, kde-format
msgid "Input"
msgstr "Sisend"

#: package/contents/ui/DeviceItem.qml:286
#, kde-format
msgid "Audio"
msgstr "Heli"

#: package/contents/ui/DeviceItem.qml:289
#, kde-format
msgid "Network"
msgstr "Võrk"

#: package/contents/ui/DeviceItem.qml:293
#, kde-format
msgid "Other device"
msgstr "Muu seade"

#: package/contents/ui/DeviceItem.qml:300 package/contents/ui/main.qml:70
#: package/contents/ui/main.qml:80
#, kde-format
msgid "%1% Battery"
msgstr "Aku %1%"

#: package/contents/ui/DeviceItem.qml:311
#, kde-format
msgctxt "Notification when the connection failed due to Failed:HostIsDown"
msgid "The device is unreachable"
msgstr "Seade ei ole saadaval"

#: package/contents/ui/DeviceItem.qml:313
#, kde-format
msgctxt "Notification when the connection failed due to Failed"
msgid "Connection to the device failed"
msgstr "Ühendumine seadmega nurjus"

#: package/contents/ui/DeviceItem.qml:317
#, kde-format
msgctxt "Notification when the connection failed due to NotReady"
msgid "The device is not ready"
msgstr "Seade ei ole valmis"

#: package/contents/ui/DeviceItem.qml:353
#, kde-format
msgctxt ""
"@label %1 is human-readable device name, %2 is low-level device address"
msgid "%1 (%2)"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:59
#, kde-format
msgid "Enable"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:147
#, fuzzy, kde-format
#| msgid "No Bluetooth Adapters Available"
msgid "No Bluetooth adapters available"
msgstr "Bluetoothi adaptereid pole saada"

#: package/contents/ui/FullRepresentation.qml:149
#, kde-format
msgid "Bluetooth is disabled"
msgstr "Bluetooth on välja lülitatud"

#: package/contents/ui/FullRepresentation.qml:151
#, fuzzy, kde-format
#| msgid "No Devices Found"
msgid "No devices found"
msgstr "Seadmeid ei leitud"

#: package/contents/ui/main.qml:48
#, kde-format
msgid "Bluetooth"
msgstr "Bluetooth"

#: package/contents/ui/main.qml:51
#, fuzzy, kde-format
#| msgid "Bluetooth is disabled"
msgid "Bluetooth is disabled; middle-click to enable"
msgstr "Bluetooth on välja lülitatud"

#: package/contents/ui/main.qml:55
#, kde-format
msgid "No adapters available"
msgstr "Adapterid puuduvad"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "Bluetooth is offline"
msgstr "Bluetooth ei ole võrgus"

#: package/contents/ui/main.qml:60
#, kde-format
msgid "Middle-click to disable Bluetooth"
msgstr ""

#: package/contents/ui/main.qml:63
#, kde-format
msgid "No connected devices"
msgstr "Ühendatud seadmed puuduvad"

#: package/contents/ui/main.qml:68
#, kde-format
msgid "%1 connected"
msgstr "%1 ühendatud"

#: package/contents/ui/main.qml:75
#, kde-format
msgctxt "Number of connected devices"
msgid "%1 connected device"
msgid_plural "%1 connected devices"
msgstr[0] "%1 ühendatud seade"
msgstr[1] "%1 ühendatud seadet"

#: package/contents/ui/main.qml:137
#, fuzzy, kde-format
#| msgid "Add New Device"
msgid "Add New Device…"
msgstr "Lisa uus seade"

#: package/contents/ui/main.qml:144 package/contents/ui/Toolbar.qml:32
#, kde-format
msgid "Enable Bluetooth"
msgstr "Lülita Bluetooth sisse"

#: package/contents/ui/main.qml:156
#, fuzzy, kde-format
#| msgid "Configure &Bluetooth..."
msgid "Configure &Bluetooth…"
msgstr "Seadista &Bluetoothi ..."

#~ msgid "Bluetooth is Disabled"
#~ msgstr "Bluetooth on välja lülitatud"

#~ msgid "Add New Device..."
#~ msgstr "Lisa uus seade..."

#~ msgid "Connected devices"
#~ msgstr "Ühendatud seadmed"

#~ msgid "Available devices"
#~ msgstr "Saadaolevad seadmed"

#~ msgid "Configure Bluetooth..."
#~ msgstr "Seadista Bluetoothi ..."
