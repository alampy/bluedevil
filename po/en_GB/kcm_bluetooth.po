# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the bluedevil package.
#
# Steve Allewell <steve.allewell@gmail.com>, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: bluedevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-18 00:38+0000\n"
"PO-Revision-Date: 2023-06-17 12:01+0100\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.03.70\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Steve Allewell"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "steve.allewell@gmail.com"

#: bluetooth.cpp:102
#, kde-format
msgctxt "DeviceName Network (Service)"
msgid "%1 Network (%2)"
msgstr "%1 Network (%2)"

#: ui/Device.qml:70 ui/main.qml:237
#, kde-format
msgid "Disconnect"
msgstr "Disconnect"

#: ui/Device.qml:70 ui/main.qml:237
#, kde-format
msgid "Connect"
msgstr "Connect"

#: ui/Device.qml:102
#, kde-format
msgid "Type:"
msgstr "Type:"

#: ui/Device.qml:108
#, kde-format
msgid "%1%"
msgstr ""

#: ui/Device.qml:112
#, kde-format
msgid "Battery:"
msgstr ""

#: ui/Device.qml:117 ui/General.qml:42
#, kde-format
msgid "Address:"
msgstr "Address:"

#: ui/Device.qml:122
#, kde-format
msgid "Adapter:"
msgstr "Adapter:"

#: ui/Device.qml:128 ui/General.qml:36
#, kde-format
msgid "Name:"
msgstr "Name:"

#: ui/Device.qml:132
#, kde-format
msgid "Trusted"
msgstr "Trusted"

#: ui/Device.qml:138
#, kde-format
msgid "Blocked"
msgstr "Blocked"

#: ui/Device.qml:144
#, kde-format
msgid "Send File"
msgstr "Send File"

#: ui/Device.qml:151
#, kde-format
msgid "Setup NAP Network…"
msgstr "Setup NAP Network…"

#: ui/Device.qml:158
#, kde-format
msgid "Setup DUN Network…"
msgstr "Setup DUN Network…"

#: ui/General.qml:19
#, kde-format
msgid "Settings"
msgstr "Settings"

#: ui/General.qml:28
#, kde-format
msgid "Device:"
msgstr "Device:"

#: ui/General.qml:46
#, kde-format
msgid "Enabled:"
msgstr "Enabled:"

#: ui/General.qml:52
#, kde-format
msgid "Visible:"
msgstr "Visible:"

#: ui/General.qml:66
#, kde-format
msgid "On login:"
msgstr "On login:"

#: ui/General.qml:67
#, kde-format
msgid "Enable Bluetooth"
msgstr "Enable Bluetooth"

#: ui/General.qml:77
#, kde-format
msgid "Disable Bluetooth"
msgstr "Disable Bluetooth"

#: ui/General.qml:87
#, kde-format
msgid "Restore previous status"
msgstr "Restore previous status"

#: ui/General.qml:106
#, kde-format
msgid "When receiving files:"
msgstr "When receiving files:"

#: ui/General.qml:108
#, kde-format
msgid "Ask for confirmation"
msgstr "Ask for confirmation"

#: ui/General.qml:117
#, kde-format
msgid "Accept for trusted devices"
msgstr "Accept for trusted devices"

#: ui/General.qml:127
#, kde-format
msgid "Always accept"
msgstr "Always accept"

#: ui/General.qml:137
#, kde-format
msgid "Save files in:"
msgstr "Save files in:"

#: ui/General.qml:161 ui/General.qml:172
#, kde-format
msgid "Select folder"
msgstr "Select folder"

#: ui/main.qml:27
#, kde-format
msgctxt "@action: button as in, 'enable Bluetooth'"
msgid "Enabled"
msgstr "Enabled"

#: ui/main.qml:43
#, kde-format
msgid "Add New Device…"
msgstr "Add New Device…"

#: ui/main.qml:49
#, kde-format
msgid "Configure…"
msgstr "Configure…"

#: ui/main.qml:88
#, kde-format
msgid "Forget this Device?"
msgstr "Forget this Device?"

#: ui/main.qml:89
#, kde-format
msgid "Are you sure you want to forget \"%1\"?"
msgstr "Are you sure you want to forget \"%1\"?"

#: ui/main.qml:100
#, kde-format
msgctxt "@action:button"
msgid "Forget Device"
msgstr "Forget Device"

#: ui/main.qml:107
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Cancel"

#: ui/main.qml:168
#, kde-format
msgid "No Bluetooth adapters found"
msgstr "No Bluetooth adapters found"

#: ui/main.qml:169
#, kde-format
msgid "Connect an external Bluetooth adapter"
msgstr ""

#: ui/main.qml:178
#, kde-format
msgid "Bluetooth is disabled"
msgstr "Bluetooth is disabled"

#: ui/main.qml:184
#, kde-format
msgid "Enable"
msgstr "Enable"

#: ui/main.qml:194
#, kde-format
msgid "No devices paired"
msgstr "No devices paired"

#: ui/main.qml:195
#, kde-kuit-format
msgctxt "@info"
msgid "Click <interface>Add New Device…</interface> to pair some"
msgstr ""

#: ui/main.qml:217
#, kde-format
msgid "Connected"
msgstr "Connected"

#: ui/main.qml:217
#, kde-format
msgid "Available"
msgstr "Available"

#: ui/main.qml:253
#, kde-format
msgctxt "@action:button %1 is the name of a Bluetooth device"
msgid "Forget \"%1\""
msgstr "Forget \"%1\""

#: ui/main.qml:284
#, kde-format
msgid "%1% Battery"
msgstr ""

#~ msgctxt "This device is a Phone"
#~ msgid "Phone"
#~ msgstr "Phone"

#~ msgctxt "This device is a Modem"
#~ msgid "Modem"
#~ msgstr "Modem"

#~ msgctxt "This device is a Computer"
#~ msgid "Computer"
#~ msgstr "Computer"

#~ msgctxt "This device is of type Network"
#~ msgid "Network"
#~ msgstr "Network"

#~ msgctxt "This device is a Headset"
#~ msgid "Headset"
#~ msgstr "Headset"

#~ msgctxt "This device is a Headphones"
#~ msgid "Headphones"
#~ msgstr "Headphones"

#~ msgctxt "This device is an Audio/Video device"
#~ msgid "Multimedia Device"
#~ msgstr "Multimedia Device"

#~ msgctxt "This device is a Keyboard"
#~ msgid "Keyboard"
#~ msgstr "Keyboard"

#~ msgctxt "This device is a Mouse"
#~ msgid "Mouse"
#~ msgstr "Mouse"

#~ msgctxt "This device is a Joypad"
#~ msgid "Joypad"
#~ msgstr "Joypad"

#~ msgctxt "This device is a Graphics Tablet (input device)"
#~ msgid "Tablet"
#~ msgstr "Tablet"

#~ msgctxt "This device is a Peripheral device"
#~ msgid "Peripheral"
#~ msgstr "Peripheral"

#~ msgctxt "This device is a Camera"
#~ msgid "Camera"
#~ msgstr "Camera"

#~ msgctxt "This device is a Printer"
#~ msgid "Printer"
#~ msgstr "Printer"

#~ msgctxt ""
#~ "This device is an Imaging device (printer, scanner, camera, display, …)"
#~ msgid "Imaging"
#~ msgstr "Imaging"

#~ msgctxt "This device is a Wearable"
#~ msgid "Wearable"
#~ msgstr "Wearable"

#~ msgctxt "This device is a Toy"
#~ msgid "Toy"
#~ msgstr "Toy"

#~ msgctxt "This device is a Health device"
#~ msgid "Health"
#~ msgstr "Health"

#~ msgctxt "Type of device: could not be determined"
#~ msgid "Unknown"
#~ msgstr "Unknown"

#~ msgid "Confirm Deletion of Device"
#~ msgstr "Confirm Deletion of Device"

#~ msgid "Remove"
#~ msgstr "Remove"

#~ msgid "Bluetooth"
#~ msgstr "Bluetooth"

#~ msgid "Nicolas Fella"
#~ msgstr "Nicolas Fella"

#~ msgid "Add…"
#~ msgstr "Add…"

#~ msgctxt "This device is an Audio device"
#~ msgid "Audio"
#~ msgstr "Audio"

#~ msgid "Add..."
#~ msgstr "Add..."
